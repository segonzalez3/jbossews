package com.example.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class EmployeeTest extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        EntityManagerFactory emf =
                Persistence.createEntityManagerFactory("EmployeeService");
        EntityManager em = emf.createEntityManager();
        EmployeeService service = new EmployeeService(em);

        // Create and persist an employee
        em.getTransaction().begin();
        Employee emp = service.createEmployee(158, "John Doe", 45000);
        em.getTransaction().commit();
        out.println("Persisted " + emp);

        // Find a specific employee
        emp = service.findEmployee(158);
        out.println("<p>Found " + emp + "</p>");

        // Find all employees
        List<Employee> emps = service.findAllEmployees();
        for (Employee e : emps) {
            out.println("<p>Found employee: " + e + "</p>");
            out.println("<p>Id is: " + e.getId() + "</p>");
        }

        // Update the employee
        em.getTransaction().begin();
        emp = service.raiseEmployeeSalary(158, 1000);
        em.getTransaction().commit();
        out.println("<p>Updated " + emp + "</p>");

        // Remove an employee
        em.getTransaction().begin();
        service.removeEmployee(158);
        em.getTransaction().commit();
        out.println("<p>Removed Employee 158</p>");

        // Close the EM and EMF when done
        em.close();
        emf.close();
    }
}
