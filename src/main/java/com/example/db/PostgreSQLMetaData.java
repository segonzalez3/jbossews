package com.example.db;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgreSQLMetaData extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost:5432/postgres";
            Connection con = DriverManager.getConnection(url, "postgres", "password");
            DatabaseMetaData dbmd = con.getMetaData();

            out.println("<p>");
            out.println("Database Product Name : ");
            out.println(dbmd.getDatabaseProductName());
            out.println("</p>");

            out.println("<p>");
            out.println("Database Product Version : ");
            out.println(dbmd.getDatabaseProductVersion());
            out.println("</p>");

            out.println("<p>");
            out.println("Database Major Version : ");
            out.println(dbmd.getDatabaseMajorVersion());
            out.println("</p>");

            out.println("<p>");
            out.println("Database Minor Version : ");
            out.println(dbmd.getDatabaseMinorVersion());
            out.println("</p>");

            out.println("<p>");
            out.println("Driver Name : ");
            out.println(dbmd.getDriverName());
            out.println("</p>");

            out.println("<p>");
            out.println("Driver Version : ");
            out.println(dbmd.getDriverVersion());
            out.println("</p>");

            out.println("<p>");
            out.println("JDBC URL : ");
            out.println(dbmd.getURL());
            out.println("</p>");

            out.println("<p>");
            out.println("Supports Transactions : ");
            out.println(dbmd.supportsTransactions());
            out.println("</p>");

            out.println("<p>");
            out.println("Default Transaction Isolation Level : ");
            out.println(dbmd.getDefaultTransactionIsolation());
            out.println("</p>");

            out.println("<p>");
            out.println("Uses Local Files : ");
            out.println(dbmd.usesLocalFiles());
            out.println("</p>");

            con.close();
        }
        catch (ClassNotFoundException e) {
            out.println("ClassNotFoundException");
            out.println("org.postgresql.Driver : " + e);
        }
        catch (SQLException e) {
            out.println("SQLException");
            out.println(e);
        }
    }
}
