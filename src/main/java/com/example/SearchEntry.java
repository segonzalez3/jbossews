package com.example;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;

@Path("results")
public class SearchEntry {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getResult(@QueryParam("car") String car,
                            @QueryParam("price") String price)
            throws Exception {

        Entry entry = new Entry();
        entry.setModel("Honda Civic");
        entry.setYear("2013");
        entry.setPrice("$16,000");
        entry.setContact("Jose Perez");
        entry.setLocation("San Juan");

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(entry);
    }
}

class Entry {
    String model;
    String year;
    String price;
    String contact;
    String location;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
