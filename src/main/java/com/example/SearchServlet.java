package com.example;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class SearchServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response)
            throws ServletException, IOException {

        String selector;
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        // Test car parameter.
        selector = request.getParameter("car");
        if (selector.equals("")) {
            selector = "Any make";
        }
        out.println(selector + " selected");

        // Test price parameter.
        selector = request.getParameter("price");
        if (selector.equals("")) {
            selector = "Any price";
        }
        out.println(selector + " selected");
    }
}
