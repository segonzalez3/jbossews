function listener() {
    var select;

    // Get car selection.
    select = document.getElementById("car");
    var car = select.options[select.selectedIndex].value;

    // Get price selection.
    select = document.getElementById("price");
    var price = select.options[select.selectedIndex].value;

    // Clear window.
    document.documentElement.removeChild(document.body);

    // Create new body.
    var body = document.createElement("body");
    document.documentElement.appendChild(body);

    // Make server request.
    var query = "?car=" + car + "&price=" + price;
    var url = "http://localhost:8080/webapi/results" + query;

    var request = new XMLHttpRequest();
    request.open("GET", url, false);
    request.send(null);

    // Generate table with data.
    var obj = JSON.parse(request.responseText);

    tableGenerator(JSON.parse(request.responseText));
}

function tableGenerator(entries) {
    var body = document.body;

    var table = document.createElement("table");
    var tbody = document.createElement("tbody");

    for (var i = 0; i < 1; i++) {
        var tr = document.createElement("tr");

        for (var iterator in entries) {
            var td = document.createElement("td");
            var text = document.createTextNode(entries[iterator]);

            td.appendChild(text);
            tr.appendChild(td);
        }

        tbody.appendChild(tr);
    }

    table.appendChild(tbody);

    body.appendChild(table);
}
